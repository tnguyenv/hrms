CREATE DATABASE NOVAHOTEL
USE NOVAHOTEL
CREATE TABLE KHACHHANG
(
	MAKH VARCHAR(20) PRIMARY KEY,
	HOTEN 	VARCHAR(50) ,
	DIACHI VARCHAR(50),
	DIENTHOAI VARCHAR(50),
	CMND VARCHAR(50),
	QUOCTICH  VARCHAR(50),
	
	
)

CREATE TABLE DANHSACHPHONG
(
	PHONG 	VARCHAR(50) PRIMARY KEY,
	LOAIPHONG VARCHAR(50),
	DONGIA	INT,
	TINHTRANG VARCHAR(50),
)
CREATE TABLE DSDICHVU
(
	MADV	VARCHAR(50)PRIMARY KEY,
	TENDV 	VARCHAR(50) ,	
	DVT	VARCHAR(50),
	DONGIA	INT,
	
)
CREATE TABLE DANGKI
(
	SODK	VARCHAR(20)PRIMARY KEY,
	NGAYDK 	SMALLDATETIME ,
	MAKH  VARCHAR(20) FOREIGN KEY REFERENCES KHACHHANG(MAKH),
	NGAYDEN	SMALLDATETIME ,
	NGAYDI 	SMALLDATETIME ,
	PHONG	VARCHAR(50)FOREIGN KEY REFERENCES DANHSACHPHONG(PHONG),
	SONGUOI	INT,
	DUATRUOC INT,
	HOTEN VARCHAR(50),
	
)

CREATE TABLE NHANPHONG
(
	SODK	VARCHAR(20)FOREIGN KEY REFERENCES DANGKI(SODK),
	MAKH  VARCHAR(20) FOREIGN KEY REFERENCES KHACHHANG(MAKH),
	
	HOTEN VARCHAR(50),
	PHONG VARCHAR(50)FOREIGN KEY REFERENCES DANHSACHPHONG(PHONG),
	NGAYNHAN	SMALLDATETIME,
	 CONSTRAINT PK_NHAN PRIMARY KEY(SODK,MAKH), 
	
)

CREATE TABLE TRAPHONG
(
	SODK	VARCHAR(20)FOREIGN KEY REFERENCES DANGKI(SODK),
	MAKH  VARCHAR(20) FOREIGN KEY REFERENCES KHACHHANG(MAKH),
	HOTEN VARCHAR(50),
	PHONG	VARCHAR(50)FOREIGN KEY REFERENCES DANHSACHPHONG(PHONG),
	NGAYNHAN	SMALLDATETIME,
	NGAYTRA	SMALLDATETIME,
	 CONSTRAINT PK_TRA PRIMARY KEY(SODK,MAKH), 
	
)

CREATE TABLE HDDICHVU
(
	
	SOHD VARCHAR (20) FOREIGN KEY REFERENCES HDPHONG(SOHD),
	MADV    VARCHAR(50)FOREIGN KEY REFERENCES DSDICHVU(MADV),
	TENDV VARCHAR(50),
	MAKH VARCHAR(20)FOREIGN KEY REFERENCES KHACHHANG(MAKH),
	HOTEN VARCHAR(50),
	PHONG VARCHAR(50)FOREIGN KEY REFERENCES DANHSACHPHONG(PHONG),
	NGAYSD	SMALLDATETIME,
	SL INT,
	DONGIA INT,
	MUCGIAM VARCHAR(20),
	TOTAL INT,
	DUATRUOC INT,
	CONLAI INT,
	CONSTRAINT PK_SOHD PRIMARY KEY(SOHD),
	
	
)

CREATE TABLE HDPHONG
(
	SOHD VARCHAR (20) PRIMARY KEY,
	SODK	VARCHAR(20)FOREIGN KEY REFERENCES DANGKI(SODK),
	MAKH  VARCHAR(20) FOREIGN KEY REFERENCES KHACHHANG(MAKH),
	HOTEN VARCHAR(50),
	QUOCTICH VARCHAR(50),
	PHONG VARCHAR(50)FOREIGN KEY REFERENCES DANHSACHPHONG(PHONG),
	SONGUOI INT,
	SONGAY	INT,
	DONGIA INT,
	TIENPHONG INT,
	TIENDV INT,
	PHUTHU VARCHAR(20),
	TOTAL INT,
	DUATRUOC INT,
	CONLAI INT,
)

SELECT * FROM HDPHONG
SELECT * FROM HDDICHVU
INSERT INTO HDPHONG VALUES('P01','456','KH03','LAM TRAN THUY','VIET NAM','202',2,12,500,6000,200,0,6200,500,5700)
INSERT INTO HDPHONG VALUES('P03','2302','KH04','LY DUC','VIET NAM','404',1,10,200,2000,200,0,2200,100,2100)
INSERT INTO HDDICHVU VALUES('P03','DT','DIENTHOAI','KH04','LY DUC','404','3/6/2007',12,500,2,6200,500,5700)
SELECT * FROM KHACHHANG
SET DATEFORMAT DMY
INSERT INTO KHACHHANG VALUES('KH01','NGUYEN VAN TEO','TAM QUAN ,BINH DINH','12344512','7373737324','VIET NAM') 
INSERT INTO KHACHHANG VALUES('KH02','DAVID','MANCHESTER ANH','1234564','7361651','ANH')
INSERT INTO KHACHHANG VALUES('KH03','NGUYEN THI BICH VAN','AN NHON ,BINH DINH','12344656','7315564','VIET NAM')
INSERT INTO KHACHHANG VALUES('KH04','LY DUC','LE CHAN ,HAI PHONG','1289181','796546516','VIET NAM')

INSERT INTO DANHSACHPHONG VALUES ('101','VIP',500,'TRONG')
INSERT INTO DANHSACHPHONG VALUES ('201','VIP',500,'TRONG')
INSERT INTO DANHSACHPHONG VALUES ('303','THUONG',300,'DATHUE')
INSERT INTO DANHSACHPHONG VALUES ('404','BINH DAN',200,'DATHUE')


INSERT INTO DSDICHVU VALUES ('DT','DIENTHOAI','PHUT',2)

INSERT INTO DSDICHVU VALUES ('XC','THUE XE','NGAY',500)
INSERT INTO DSDICHVU VALUES ('GU','GIAT UI','KG',5)
SELECT * FROM DSDICHVU


INSERT INTO PTHUEPHONG VALUES ('103','NGUYEN AN','200456','VIP',1,'UC','34 LUS SYSNEY',500)
INSERT INTO PTHUEPHONG VALUES ('104','NGUYEN HONG VIET','211880','VIP',2,'VIETNAM','315 NHA A13 ','25/3/2008',500)
INSERT INTO PTHUEPHONG VALUES ('202','LY AN','200479','VIP',1,'TRUNG QUOC','34 BACKINH',0)
INSERT INTO PTHUEPHONG VALUES ('204','NGUYEN DO','20078','THUONG',1,'VIETNAM','310 LECHAN HAIPHONG',300)
INSERT INTO PTHUEPHONG VALUES ('304','VAN TOAN','211456','THUONG',3,'VIETNAM','12 VOTHISAU Q3',500)

INSERT INTO PTDICHVU VALUES ('NGUYEN AN','DT','DIENTHOAI','103',20,'PHUT',2,0)
INSERT INTO PTDICHVU VALUES ('NGUYEN HONG VIET','XC','THUEXE','104',3,'NGAY',500,500)
INSERT INTO PTDICHVU VALUES ('NGUYEN DO','AU','ANUONG','204',6,'BUA',100,200)

INSERT INTO HOADON VALUES (31208,'103','NGUYEN AN',3000,40,3040,'TIEN MAT',NULL)
ALTER TABLE DSDICHVU ALTER COLUMN DVT VARCHAR(50)
SELECT * FROM DSDICHVU
SELECT * FROM HOADON
