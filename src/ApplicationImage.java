import java.awt.*;
import java.awt.event.*;

import javax.swing.JFrame;
public class ApplicationImage extends JFrame{
	Image img;
public ApplicationImage(String title)
{
	super(title);
	img=getToolkit().getImage("c:\\4.gif");
	
}
public static void main(String args[])
{
	ApplicationImage ai=new ApplicationImage("hien anh");
	ai.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent event)
		{
			System.exit(0);
		}
	});
	ai.setSize(300,300);
	ai.setVisible(true);
	
}
public void paint(Graphics g)
{
	g.drawImage(img,0,0,this);
}
}
