
import java.awt.*;
import java.applet.*;

public class Drawpicture extends Applet {
	private Image img;
	private static final long serialVersionUID = -2326675758694413696L;

	/** Creates a new instance of Drawpicture */
	public Drawpicture() {
		img = getImage(getDocumentBase(), "c:\\4.gif");
	}

	public void init() {
		
	}

	public void paint(Graphics g) {
		g.drawImage(img, 0, 0, this);
	}

	public static void main(String[] args) {
		new Drawpicture();
	}
}
