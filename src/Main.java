

/**
 *
 * @author nova
 */
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.sql.*;
import javax.swing.JFrame;

import novahotel.Cashier;
import novahotel.CustomerList;
import novahotel.Login;
import novahotel.Makeroombill;
import novahotel.Makeservicebill;
import novahotel.PayRoom;
import novahotel.Reciveroom;
import novahotel.Reportlistcustumerhireroom;
import novahotel.Reportlistcustumerpayroom;
import novahotel.Reportlistcustumeruseservice;
import novahotel.Searchcustumerinroom;
import novahotel.Searchcustumername;
import novahotel.Searchroombil;
import novahotel.Searchservicebil;
import novahotel.SetupDatabase;
import novahotel.Updateroom;
import novahotel.RegisterRoom;
import novahotel.roomlist;

class Main extends JFrame{
    
    /**
	 * 
	 */
	public void showSimpleDialog(JFrame f) {
		final JDialog d = new JDialog(f, "Click OK", true);
		d.setSize(200, 150);
		JLabel l = new JLabel("Click OK after you read this", JLabel.CENTER);
		d.getContentPane().setLayout(new BorderLayout());
		d.getContentPane().add(l, BorderLayout.CENTER);
		JButton b = new JButton("OK");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				d.setVisible(false);
				d.dispose();
			}
		});
		JPanel p = new JPanel(); // Flow layout will center button.
		p.add(b);
		d.getContentPane().add(p, BorderLayout.SOUTH);
		d.setLocationRelativeTo(f);
		d.setVisible(true);
	}
	
	private static final long serialVersionUID = 1L;
	/** Creates a new instance of Main */
    JMenuBar mb ;
    JMenu mmanagement,mreport,msearch,msystem,mintroduce,mcustumer,mroom,mservice,mcashier,msearcustumer;
    JMenuItem misearchroom,miregister,miqlregister,miqldskhach,miqlpayroom,misearroombil,misearservicebil,
            miqlreciveroom,miqluseservice,mithutien,mireport,miupdateroom,miroombill,miservicebill
            ,misearname,misearroom,mireservice,mirepayroom,mirereciveroom, miSysLogin, miSysSetup, miSysExit;
    JLabel lb,lb2;
    Font font=new Font("Arial",Font.BOLD,14);
    Font flabel=new Font("Times New Roman",Font.ITALIC,34);
    
    public Main() {
        this.CreateMenu();
	//this.setSize(500,500);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setTitle("QUAN L� KH�CH SAN - SINH VI�N THUC HIEN : NGUYEN HONG VIET MSSV : 06520566");
        this.setFont(font);
        this.setVisible(true);
    }
    
    public void CreateMenu() {
		lb = new JLabel("WELCOME TO INNOVA HOTEL");
		lb.setFont(flabel);
		getContentPane().add(lb, BorderLayout.PAGE_START);
		mb = new JMenuBar();
		mmanagement = new JMenu("MANAGEMENT");
		mmanagement.setFont(font);
		mreport = new JMenu("REPORT");
		mreport.setFont(font);
		msearch = new JMenu("SEARCH");
		msearch.setFont(font);
		msystem = new JMenu("SYSTEM");
		msystem.setFont(font);
		mintroduce = new JMenu("INTRODUCTION");
		mintroduce.setFont(font);
		mcustumer = new JMenu("CUSTUMER");
		mcustumer.setFont(font);
		mroom = new JMenu("Quan li phong");
		mroom.setFont(font);
		mservice = new JMenu("Quan li dich vu");
		mservice.setFont(font);
		mcashier = new JMenu("Thu ngan");
		mcashier.setFont(font);
		misearchroom = new JMenuItem("Tra cuu phong");
		misearchroom.setFont(font);
		miregister = new JMenuItem("Dang ky");
		miregister.setFont(font);

		miqldskhach = new JMenuItem("Thong tin khach dang ki ");
		miqldskhach.setFont(font);
		miqlreciveroom = new JMenuItem("Quan li nhan phong");
		miqlreciveroom.setFont(font);

		miqlpayroom = new JMenuItem("Quan li tra phong");
		miqlpayroom.setFont(font);
		miqlregister = new JMenuItem("Quan li dat phong");
		miqlregister.setFont(font);
		miqluseservice = new JMenuItem("Danh sach khach use service");
		miqluseservice.setFont(font);
		mithutien = new JMenuItem("Thu tien");
		mithutien.setFont(font);
		mireport = new JMenuItem("Lap bao cao thang");
		mireport.setFont(font);
		miupdateroom = new JMenuItem("Update phong");
		miupdateroom.setFont(font);
		miroombill = new JMenuItem("Lap hoa don phong");
		miroombill.setFont(font);
		miservicebill = new JMenuItem("Lap hoa don dich vu");
		miservicebill.setFont(font);

		//Khai bao cho menu SYSTEM
		miSysLogin = new JMenuItem("Dang nhap he thong");
		miSysSetup = new JMenuItem("Cai dat co so du lieu");
		miSysExit = new JMenuItem("Thoat");
		//khai bao cho menu search
		msearcustumer = new JMenu("Khach hang");
		msearcustumer.setFont(font);
		misearname = new JMenuItem("Tim theo ten");
		misearname.setFont(font);
		misearroom = new JMenuItem("Tim theo phong");
		misearroom.setFont(font);
		misearroombil = new JMenuItem("Tim hoa don phong");
		misearroombil.setFont(font);
		misearservicebil = new JMenuItem("Tim hoa don dich vu");
		misearservicebil.setFont(font);
		//khai bao cho menu report

		mireservice = new JMenuItem("Danh sach khach use dich vu trong ngay");
		mireservice.setFont(font);
		mirereciveroom = new JMenuItem("Danh sach khach nhan phong trong ngay");
		mirereciveroom.setFont(font);
		mirepayroom = new JMenuItem("Danh sach khach tra phong trong ngay");
		mirepayroom.setFont(font);

		mb.add(msystem);
		mb.add(mcustumer);
		mb.add(mmanagement);
		mb.add(mreport);
		mb.add(msearch);
		mb.add(mintroduce);
		mb.setBackground(Color.green);
		mmanagement.add(mroom);
		mmanagement.add(mservice);
		mmanagement.add(mcashier);

		msystem.add(miSysLogin);
		msystem.add(miSysSetup);
		msystem.addSeparator();
		msystem.add(miSysExit);

		mroom.add(miqldskhach);
		mroom.add(miupdateroom);
		mroom.add(miqlregister);
		mroom.add(miqlreciveroom);
		mroom.add(miqlpayroom);
		mroom.add(miroombill);

		mservice.add(miservicebill);
		mservice.add(miqluseservice);

		mcashier.add(mithutien);
		mcashier.add(mireport);

		mcustumer.add(misearchroom);
		mcustumer.add(miregister);

		msearch.add(msearcustumer);
		msearch.add(misearroombil);
		msearch.add(misearservicebil);
		msearcustumer.add(misearname);
		msearcustumer.add(misearroom);

		mreport.add(mirereciveroom);
		mreport.add(mireservice);
		mreport.add(mirepayroom);

		miSysLogin.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				try{
					new Login();
				} catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		miSysExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					JFrame f = new JFrame();
					final JDialog d = new JDialog(f, "Xac nhan",true);
					d.setSize(250, 120);
					JLabel l = new JLabel("Ban co that su muon thoat chuong trinh?", JLabel.CENTER);
					d.getContentPane().setLayout(new BorderLayout());
					d.getContentPane().add(l, BorderLayout.CENTER);
					JButton bOK = new JButton("OK");
					JButton bCancel = new JButton("Cancel");
					
					bOK.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent ev) {
							d.setVisible(false);
							System.exit(0);
						}
					});
					
					bCancel.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent ev){
							d.setVisible(true);
							d.dispose();
						}
					});
					JPanel p = new JPanel(); // Flow layout will center button.
					p.add(bOK);
					p.add(bCancel);
					
					d.getContentPane().add(p, BorderLayout.SOUTH);
					d.setLocationRelativeTo(f);
					d.setVisible(true);
				
				} catch (Exception exp) {

				}
			}
		});

		miSysSetup.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				try{
					new SetupDatabase();
				} catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		miqlregister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new RegisterRoom();
				} catch (Exception ex) {
				}
			}
		});
		
		misearchroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new roomlist();
				} catch (Exception ex) {
				}
			}
		});

		miregister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new RegisterRoom();
				} catch (Exception ex) {
				}
			}
		});

		miqldskhach.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new CustomerList();
				} catch (Exception ex) {
				}
			}
		});
		
		miqlreciveroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Reciveroom();
				} catch (Exception ex) {
				}
			}
		});
		
		miqlpayroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new PayRoom();
				} catch (Exception ex) {
				}
			}
		});
		
		miupdateroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Updateroom();
				} catch (Exception ex) {
				}
			}
		});
		
		miroombill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Makeroombill();
				} catch (Exception ex) {
				}
			}
		});
		
		miservicebill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Makeservicebill();
				} catch (Exception ex) {
				}
			}
		});
		/*    miqluseservice.addActionListener(new ActionListener() {
		
		        public void actionPerformed(ActionEvent e) {
		        	
		            try{
		                new Custumeruseservicelist();
		            }
		            catch(Exception ex){
		        }
		        }
		    });
		 */
		
		mithutien.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Cashier();
				} catch (Exception ex) {
				}
			}
		});

		/*
		   mireport.addActionListener(new ActionListener() {
		       public void actionPerformed(ActionEvent e) {
		           try{
		               new thuxuat();
		           }
		           catch(Exception ex){
		       }
		       }
		   });
		 */
		mirereciveroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Reportlistcustumerhireroom();
				} catch (Exception ex) {
				}
			}
		});
		
		mireservice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Reportlistcustumeruseservice();
				} catch (Exception ex) {
				}
			}
		});
		
		mirepayroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Reportlistcustumerpayroom();
				} catch (Exception ex) {
				}
			}
		});
		
		misearname.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Searchcustumername();
				} catch (Exception ex) {
				}
			}
		});
		
		misearroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Searchcustumerinroom();
				} catch (Exception ex) {
				}
			}
		});
		
		misearroombil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Searchroombil();
				} catch (Exception ex) {
				}
			}
		});
		
		misearservicebil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new Searchservicebil();
				} catch (Exception ex) {
				}
			}
		});
		
		this.setJMenuBar(mb);
		// mb.setBackground(Color.GREEN);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
 
    public static void main(String[] args) {
        // TODO code application logic here
        Main mnu=new Main();
    }
    
}
