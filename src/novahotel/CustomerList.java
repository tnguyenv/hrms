/*
 * CustomerList.java
 *
 * Created on May 22, 2006, 2:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package novahotel;

/**
 *
 * @author nova
 */
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

public class CustomerList extends JFrame implements ActionListener{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Font font=new Font("Arial",Font.BOLD,14);
     Font fontla=new Font("Times New Roman",Font.BOLD,22);
     JButton btnAdd=new JButton("Them");
     JButton btnDelete=new JButton("Xoa");
     JButton btnRepair=new JButton("Sua");
     JButton btnExit=new JButton("Thoat");
     
     JTextField  txtMakh=new JTextField(20);
     JTextField txtName=new JTextField(20);
        JTextField txtAdress=new JTextField(20);
        JTextField txtPhone=new JTextField(20);
        JTextField txtCmnd=new JTextField(20);
        JTextField txtCountry=new JTextField(20);
        
        JLabel lbMakh=new JLabel("Ma kh�ch h�ng");
        JLabel lbName=new JLabel("Ho t�n");
        JLabel lbAdress=new JLabel("Dia chi");
        JLabel lbPhone=new JLabel("So DT");
        JLabel lbCmnd=new JLabel("CMND");
        JLabel lbCountry=new JLabel("Quoc tich");
       
        JLabel lblist=new JLabel("DANH  SACH  KHACH  HANG ");
       
        
        JPanel pnstart,pnend;
        String sql;
        public Statement st;
        Connection con;
        public ResultSet rs;
        public ResultSetMetaData rsmd;
        JTable tableData ;//the cells is  databaseManager.rs ; column-->
        String [] columnNames   = {"MA KH","HO TEN","DIA CHI","DIEN THOAI","CMND","QUOC TICH"};
        public Object [][] objectData  = new Object[50][6];
    
    
    /** Creates a new instance of CustomerList */
    public CustomerList() {
     rs=null;
        rsmd=null;
      getContentPane().setLayout(new BorderLayout());
      //data=new dulieu();
      pnstart =new JPanel();
      pnstart.setLayout(new GridLayout(9,2));
      lbMakh.setFont(font);
      pnstart.add(lbMakh);
      pnstart.add(txtMakh);
      lbName.setFont(font);
      pnstart.add(lbName);
      pnstart.add(txtName);
      lbAdress.setFont(font);
      pnstart.add(lbAdress);
      pnstart.add(txtAdress);
      lbPhone.setFont(font);
      pnstart.add(lbPhone);
      pnstart.add(txtPhone);
      lbCmnd.setFont(font);
      pnstart.add(lbCmnd);
      pnstart.add(txtCmnd);
      lbCountry.setFont(font);
      pnstart.add(lbCountry);
      pnstart.add(txtCountry);
      
      pnstart.add(lblist);
      lblist.setFont(fontla);
      
      pnend=new JPanel();
      pnend.add(btnAdd);
      //new xuatphieuthue();
      pnend.add(btnDelete);
      pnend.add(btnRepair);
      pnend.add(btnExit);
      
      btnDelete.addActionListener(this);
      btnAdd.addActionListener(this);
      btnExit.addActionListener(this);
      btnRepair.addActionListener(this);
     try 
	{
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		String url="jdbc:odbc:novahotel";
		
		con=DriverManager.getConnection(url);
		st=con.createStatement();
		//sql="insert into thu dsphong('503','VIP',500,'TRONG')";
		//st.executeUpdate(sql);
      }
      catch(SQLException e)
	{
		System.out.println(e);
	 }
	 catch(ClassNotFoundException e)
	 {
	 	System.out.println(e);
	 }
      
      getContentPane().add(pnstart,BorderLayout.PAGE_START);
      getContentPane().add(pnend,BorderLayout.PAGE_END);
        xuatbangdl();
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      pack();
     // this.setExtendedState(JFrame.MAXIMIZED_BOTH);
      this.setSize(800,500);
      setVisible(true);
   
  
    }
    public void getTable(String nameTable)  {
        try{
            rs=st.executeQuery("select * from  "+ nameTable);
            rsmd= rs.getMetaData();
        }
        catch(SQLException ex)
        {
            System.out.println(ex);
        }
    }
    public void xuatbangdl()
    {
        try{
        for(int i =0;i<50;i++) {
           for (int j =0 ;j<6;j++)
             objectData[i][j]="";
                     } 
                getTable("khachhang");
                int i =0;
                while(rs.next())
                {
                objectData[i][0]=rs.getString(1); 
                objectData[i][1]=rs.getString(2);
                objectData[i][2]=rs.getString(3);
                objectData[i][3]=rs.getString(4);
                objectData[i][4]=rs.getString(5); 
                objectData[i++][5]=rs.getString(6);
                
          //objectData[i++][4]=data.rs.getString(5);
         }
                tableData = new JTable(objectData,columnNames);
                Container contenPane=this.getContentPane();
                contenPane.add(new JScrollPane(tableData),"Center");
                this.setBounds(200,200 ,800, 500);
                tableData.setVisible(true);
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
    }
     public void actionPerformed(ActionEvent e)
    {
    	try{
    		if(e.getSource().equals(btnAdd))
    		{
                    
            	String sq="insert into khachhang values ('"+txtMakh.getText();sq=sq+"','"+txtName.getText()
                ;sq=sq+"','"+txtAdress.getText();sq=sq+"','"+ txtPhone.getText();sq=sq+"','" +txtCmnd.
                getText();sq=sq +"','" +txtCountry.getText() + "')";
                st.executeUpdate(sq);
                
                xuatbangdl();
                
    			
    		JOptionPane.showMessageDialog(this,"Insert success in database!"); 
                
    		}
              
    		else if (e.getSource().equals(btnDelete))
                {
                  
                    //String sq="delete from pthuephong where hoten='"+txthten.getText()+"'";
                    String sq= "delete  from hddichvu where makh='"+txtMakh.getText()+"'";                       
                    st.executeUpdate(sq);
                    String sq1= "delete  from hdphong where makh='"+txtMakh.getText()+"'";                       
                    st.executeUpdate(sq1);
                    String sq2= "delete  from traphong where makh='"+txtMakh.getText()+"'";                       
                    st.executeUpdate(sq2);
                    String sq3= "delete  from nhanphong where makh='"+txtMakh.getText()+"'";                       
                    st.executeUpdate(sq1);
                    String sq4= "delete  from dangki where makh='"+txtMakh.getText()+"'";                       
                    st.executeUpdate(sq4);
                    String sq5= "delete  from khachhang where makh='"+txtMakh.getText()+"'";                       
                    st.executeUpdate(sq5);
                    xuatbangdl();
                    
                }
                else if(e.getSource().equals(btnRepair))
                {
                    String sql= "update khachhang set hoten='"+txtName.getText()+"',diachi='" +
                    txtAdress.getText()+"',dienthoai='"+txtPhone.getText()+"',cmnd='"+txtCmnd.getText()+"'" +
                            ",quoctich='" +txtCountry.getText()+"' where makh= '"+txtMakh.getText()+"'"; 
                    st.executeUpdate(sql);
                    xuatbangdl();
                }
                else if(e.getSource().equals(btnExit))
                {
                	this.dispose();
                }
        }
    	catch(SQLException ce)
    	{
    		System.out.println(ce);
    	}
    	
    	
    }
    
}
