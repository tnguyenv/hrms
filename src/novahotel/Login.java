package novahotel;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Login extends JFrame implements ActionListener{
	private static final long serialVersionUID = 6700685950407136625L;
	JTextField txtUsername = new JTextField(20);
	JTextField txtPassword = new JTextField(20);
	JLabel lblUsername = new JLabel("Ten dang nhap:");
	JLabel lblPassword = new JLabel("Mat khau:");
	JButton btnConnect = new JButton("Ket noi");
	JButton btnQuit = new JButton("Huy bo");
	
	public Login(){
		getContentPane().setLayout(new BorderLayout());
		JPanel login = new JPanel();
		login.add(lblUsername);
		login.add(txtUsername);
		login.add(lblPassword);
		login.add(txtPassword);
		login.setLayout(new GridLayout(2,2));
		getContentPane().add(login, BorderLayout.PAGE_START);
		
		JPanel login2 = new JPanel();
		login2.add(btnConnect);
		login2.add(btnQuit);
		btnConnect.addActionListener(this);
		btnQuit.addActionListener(this);
		getContentPane().add(login2, BorderLayout.PAGE_END);
		
	    this.setSize(200,150);
		pack();
		this.setVisible(true);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource().equals(btnConnect))
		{
			
		}
		else if(e.getSource().equals(btnQuit))
		{
			
		}
	}
	
}
