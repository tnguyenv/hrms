/*
 * Makeroombill.java
 *
 * Created on May 24, 2006, 8:36 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package novahotel;

/**
 *
 * @author nova
 */
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.lang.*;
import javax.swing.table.*;
import javax.xml.transform.Result;
public class Makeroombill extends JFrame implements ActionListener{
    
    /** Creates a new instance of Makeroombill */
    Font font=new Font("Arial",Font.BOLD,14);
    Font fontla=new Font("Times New Roman",Font.BOLD,22);
    JButton btnAdd=new JButton("Them");
    JButton btnDelete=new JButton("Xoa");
    JButton btnRepair=new JButton("Sua");
    JButton btnExit=new JButton("Thoat");
    PayRoom p=new PayRoom();
    
    JTextField  txtBillcode=new JTextField(20);
    JTextField txtSodk=new JTextField(20);
    JTextField txtMakh=new JTextField(20);
    JTextField txtName=new JTextField(20);
    JTextField txtCountry=new JTextField(20);
    JTextField txtRoom=new JTextField(20);
    JTextField txtSong=new JTextField(20);
    JTextField txtSongay=new JTextField(20);
    JTextField txtUnitprice=new JTextField(20);
    JTextField txtTiendv=new JTextField(20);
    JTextField txtPhuthu=new JTextField(20);
    JTextField txtTotal=new JTextField(20);
    JTextField txtPrepay=new JTextField(20);
    JTextField txtRemain=new JTextField(20);
    
    JLabel lbBillcode=new JLabel("So hoa don");
    JLabel lbSodk=new JLabel("So dang ki");
    JLabel lbMakh=new JLabel("Ma khach hang");
    JLabel lbName=new JLabel("Ho ten ");
    JLabel lbCountry=new JLabel("Quoc tich");
    JLabel lbRoom=new JLabel("Phong");
    JLabel lbSong=new JLabel("So nguoi");
    JLabel lbSongay=new JLabel("So ngay");
    JLabel lbUnitPrice=new JLabel("Don gia");
    JLabel lbTiendv=new JLabel("Tien dich vu");
    JLabel lbPhuthu=new JLabel("Phu thu");
    JLabel lbTotal=new JLabel("Tong cong");
    JLabel lbPrepay=new JLabel("Dua truoc");
    JLabel lbRemain=new JLabel("Con lai");
    JLabel lblist=new JLabel("DANH  S�CH  HOA DON KHACH USE PHONG");
    
    
    JPanel pnstart,pnend;
    String sql;
    public Statement st;
    Connection con;
    public ResultSet rs;
    public ResultSetMetaData rsmd;
    JTable tableData ;//the cells is  databaseManager.rs ; column-->
    String [] columnNames   = {"SO HDON","SO DK","MAKH","HO TEN","QUOC TICH",
    "PHONG","SO NGUOI"," SO NGAY","DON GIA","TIEN DV","PHU THU","TONG CONG","DUA TRUOC","CON LAI"};
    public Object [][] objectData  = new Object[50][14];
    
    public Makeroombill() {
        rs=null;
        rsmd=null;
        getContentPane().setLayout(new BorderLayout());
        //data=new dulieu();
        pnstart =new JPanel();
        pnstart.setLayout(new GridLayout(15,2));
        lbBillcode.setFont(font);
        pnstart.add(lbBillcode);
        pnstart.add(txtBillcode);
        lbSodk.setFont(font);
        pnstart.add(lbSodk);
        pnstart.add(txtSodk);
        lbMakh.setFont(font);
        pnstart.add(lbMakh);
        pnstart.add(txtMakh);
        lbName.setFont(font);
        pnstart.add(lbName);
        pnstart.add(txtName);
        lbCountry.setFont(font);
        pnstart.add(lbCountry);
        pnstart.add(txtCountry);
        lbRoom.setFont(font);
        pnstart.add(lbRoom);
        pnstart.add(txtRoom);
        lbSong.setFont(font);
        pnstart.add(lbSong);
        pnstart.add(txtSong);
        lbSongay.setFont(font);
        pnstart.add(lbSongay);
        pnstart.add(txtSongay);
        lbUnitPrice.setFont(font);
        pnstart.add(lbUnitPrice);
        pnstart.add(txtUnitprice);
        lbTiendv.setFont(font);
        pnstart.add(lbTiendv);
        pnstart.add(txtTiendv);
        lbName.setFont(font);
        pnstart.add(lbPhuthu);
        pnstart.add(txtPhuthu);
        lbTotal.setFont(font);
        pnstart.add(lbTotal);
        pnstart.add(txtTotal);
        lbPrepay.setFont(font);
        pnstart.add(lbPrepay);
        pnstart.add(txtPrepay);
        lbRemain.setFont(font);
        pnstart.add(lbRemain);
        pnstart.add(txtRemain);
        pnstart.add(lblist);
        lblist.setFont(fontla);
        
        pnend=new JPanel();
        pnend.add(btnAdd);
        //new xuatphieuthue();
        pnend.add(btnDelete);
        pnend.add(btnRepair);
        pnend.add(btnExit);
        
        btnDelete.addActionListener(this);
        btnAdd.addActionListener(this);
        btnExit.addActionListener(this);
        btnRepair.addActionListener(this);
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String url="jdbc:odbc:novahotel";
            
            con=DriverManager.getConnection(url);
            st=con.createStatement();
            //sql="insert into thu dsphong('503','VIP',500,'TRONG')";
            //st.executeUpdate(sql);
        } catch(SQLException e) {
            System.out.println(e);
        } catch(ClassNotFoundException e) {
            System.out.println(e);
        }
        
        getContentPane().add(pnstart,BorderLayout.PAGE_START);
        getContentPane().add(pnend,BorderLayout.PAGE_END);
        xuatbangdl();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        //this.setSize(800,500);
        setVisible(true);
        
    }
    public void getTable(String nameTable)  {
        try{
            rs=st.executeQuery("select * from  "+ nameTable);
            rsmd= rs.getMetaData();
        } catch(SQLException ex) {
            System.out.println(ex);
        }
    }
    public void xuatbangdl() {
        try{
            for(int i =0;i<50;i++) {
                for (int j =0 ;j<14;j++)
                    objectData[i][j]="";
            }
            getTable("hdphong");
            int i =0;
            while(rs.next()) {
                objectData[i][0]=rs.getString(1);
                objectData[i][1]=rs.getString(2);
                objectData[i][2]=rs.getString(3);
                objectData[i][3]=rs.getString(4);
                objectData[i][4]=rs.getString(5);
                objectData[i][5]=rs.getString(6);
                objectData[i][6]=rs.getInt(7);
                objectData[i][7]=rs.getInt(8);
                objectData[i][8]=rs.getInt(9);
                objectData[i][9]=rs.getInt(10);
                objectData[i][10]=rs.getInt(11);
                objectData[i][11]=rs.getInt(12);
                objectData[i][12]=rs.getInt(13);
                objectData[i++][13]=rs.getInt(14);
            }
            tableData =new JTable(objectData,columnNames);
            Container contenPane=this.getContentPane();
            contenPane.add(new JScrollPane(tableData),"Center");
            this.setBounds(200,200 ,1200, 500);
            this.show(true);
        } catch(SQLException e) {
            System.out.println(e);
        }
    }
    public void actionPerformed(ActionEvent e) {
        try{
            if(e.getSource().equals(btnAdd)) {
                String hten= "select tp.hoten from traphong tp,hdphong hd where " +
                        "tp.sodk=hd.sodk and hd.sodk='"+txtSodk.getText()+"'";
                int sngay=    Integer.parseInt(txtSongay.getText());
                int dgia= Integer.parseInt(txtUnitprice.getText());
                int total=sngay * dgia;
                String sq="insert into hdphong values ('"+txtBillcode.getText();sq=sq+"','"+txtSodk.getText();sq=sq+"'" +
                ",'"+txtMakh.getText();sq=sq+"','"+txtName.getText();sq=sq+"','"+txtCountry.getText();sq=sq+"'," +
                "'"+ txtRoom.getText();sq=sq+"','" +Integer.parseInt(txtSong.getText());sq=sq+"','" +Integer.parseInt(txtSongay.getText());sq=sq+"'," +
                "'" +Integer.parseInt(txtUnitprice.getText());sq=sq+ "','" +Integer.parseInt(txtTiendv.getText());sq=sq+"'" +
                ",'" +Integer.parseInt(txtPhuthu.getText());sq=sq+"','"+total+"','" +Integer.parseInt(txtPrepay.getText())
                ;sq=sq+"','"+Integer.parseInt(txtRemain.getText())+"')";
                st.executeUpdate(sq);
                xuatbangdl();
                JOptionPane.showMessageDialog(this,"Record add !");
                                                
            }
            
            else if (e.getSource().equals(btnDelete)) {
                
                //String sq="delete from pthuephong where hoten='"+txthten.getText()+"'";
                String sq= "delete  from hddichvu where  sohd='"+txtBillcode.getText()+"'";
                st.executeUpdate(sq);
                String sql= "delete  from hdphong where  sohd='"+txtBillcode.getText()+"'";
                st.executeUpdate(sql);
                xuatbangdl();
                
            } else if(e.getSource().equals(btnRepair)) {
                String sql= "update hdphong set sodk='"+txtSodk.getText()+"',makh='" +
                        txtMakh.getText()+"',hoten='"+txtName.getText()+"',quoctich='"+txtCountry.getText()+"'," +
                        "phong='"+txtRoom.getText()+"'" + ",songuoi='"+txtSong.getText()+"',songay='"+txtSongay.getText()+"'," +
                        "dongia='"+ txtUnitprice.getText()+"',tiendv='"+txtTiendv.getText()+"'," +
                        "phuthu='"+txtPhuthu.getText()+"',total='"+txtTotal.getText()
                        +"',duatruoc='"+txtPrepay.getText()+"',conlai='"+txtRemain.getText()+"' " +
                        " where sohd= '"+txtBillcode.getText()+"'";
                st.executeUpdate(sql);
                xuatbangdl();
            } else if(e.getSource().equals(btnExit)) {
            	this.dispose();
            }
            
        } catch(SQLException ce) {
            System.out.println(ce);
        }
        
        
    }
    
}
