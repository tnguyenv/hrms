/*
 * registerroom.java
 *
 * Created on May 19, 2006, 3:31 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package novahotel;

import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.lang.*;
import javax.swing.table.*;
import javax.xml.transform.Result;
import java.util.*;

/**
 *
 * @author nova
 */
 public class RegisterRoom extends JFrame implements ActionListener{
     Font font=new Font("Arial",Font.BOLD,14);
     Font fontla=new Font("Times New Roman",Font.BOLD,22);
     JButton btnAdd=new JButton("Dang ki");
     JButton btnDelete=new JButton("Huy dang ki");
     JButton btnRepair=new JButton("Sua thong tin");
     JButton btnExit=new JButton("Thoat");
     
     JTextField  txtSodk=new JTextField(20);
     JTextField txtNgdk=new JTextField(20);
        JTextField txtMakh=new JTextField(20);
        JTextField txtHoten=new JTextField(20);
        JTextField txtNgden=new JTextField(20);
        JTextField txtNgdi=new JTextField(20);
        JTextField txtRoom=new JTextField(20);
        JTextField txtSong=new JTextField(20);
        JTextField txtDtruoc=new JTextField(20);
        JLabel lbSodk=new JLabel("So dang ki");
        JLabel lbNgdk=new JLabel("Ngay dang ki");
        JLabel lbMakh=new JLabel("Ma khach hang");
        JLabel lbHoten=new JLabel("Ho ten");
        JLabel lbNgden=new JLabel("Ngay den");
        JLabel lbNgdi=new JLabel("Ngay di");
        JLabel lbRoom=new JLabel("Phong");
        JLabel lbSong=new JLabel("So nguoi");
        JLabel lbDtruoc=new JLabel("Dua truoc");
        JLabel lblist=new JLabel("DANH  S�CH  KH�CH  H�NG  DANG K� PH�NG");
       
        
        JPanel pnstart,pnend;
        String sql;
        public Statement st;
        Connection con;
        public ResultSet rs;
        public ResultSetMetaData rsmd;
        JTable tableData ;//the cells is  databaseManager.rs ; column-->
        String [] columnNames   = {"SO DK","NGAY DK","MAKH","NGAY DEN","NGAY DI","PHONG","SO NGUOI","DUA TRUOC","HO TEN"};
        public Object [][] objectData  = new Object[50][9];
    
    /** Creates a new instance of registerroom */
    public RegisterRoom() {
         rs=null;
        rsmd=null;
      getContentPane().setLayout(new BorderLayout());
      //data=new dulieu();
      pnstart =new JPanel();
      pnstart.setLayout(new GridLayout(11,2));
      lbSodk.setFont(font);
      pnstart.add(lbSodk);
      pnstart.add(txtSodk);
      lbNgdk.setFont(font);
      pnstart.add(lbNgdk);
      pnstart.add(txtNgdk);
      lbMakh.setFont(font);
      pnstart.add(lbMakh);
      pnstart.add(txtMakh);
     
      lbNgden.setFont(font);
      pnstart.add(lbNgden);
      pnstart.add(txtNgden);
      lbNgdi.setFont(font);
      pnstart.add(lbNgdi);
      pnstart.add(txtNgdi);
      lbRoom.setFont(font);
      pnstart.add(lbRoom);
      pnstart.add(txtRoom);
      lbSong.setFont(font);
      pnstart.add(lbSong);
      pnstart.add(txtSong);
      lbDtruoc.setFont(font);
      pnstart.add(lbDtruoc);
      pnstart.add(txtDtruoc);
      lbHoten.setFont(font);
      pnstart.add(lbHoten);
      pnstart.add(txtHoten);
      pnstart.add(lblist);
      lblist.setFont(fontla);
      
      pnend=new JPanel();
      pnend.add(btnAdd);
      //new xuatphieuthue();
      pnend.add(btnDelete);
      pnend.add(btnRepair);
      pnend.add(btnExit);
      
      btnDelete.addActionListener(this);
      btnAdd.addActionListener(this);
      btnExit.addActionListener(this);
      btnRepair.addActionListener(this);
     try 
	{
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		String url="jdbc:odbc:novahotel";
		
		con=DriverManager.getConnection(url);
		st=con.createStatement();
		//sql="insert into thu dsphong('503','VIP',500,'TRONG')";
		//st.executeUpdate(sql);
      }
      catch(SQLException e)
	{
		System.out.println(e);
	 }
	 catch(ClassNotFoundException e)
	 {
	 	System.out.println(e);
	 }
      
      getContentPane().add(pnstart,BorderLayout.PAGE_START);
      getContentPane().add(pnend,BorderLayout.PAGE_END);
       xuatbangdl();
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      pack();
     this.setExtendedState(JFrame.MAXIMIZED_BOTH);
      //this.setSize(800,500);
      setVisible(true);
   
    }

    public void getTable(String nameTable)  {
        try{
            rs=st.executeQuery("select * from  "+ nameTable);
            rsmd= rs.getMetaData();
        }
        catch(SQLException ex)
        {
            System.out.println(ex);
        }
    }
    public void xuatbangdl()
    {
        try{
        for(int i =0;i<50;i++) {
           for (int j =0 ;j<9;j++)
             objectData[i][j]="";
                     } 
                getTable("dangki");
                int i =0;
                while(rs.next())
                {
                objectData[i][0]=rs.getString(1); 
                objectData[i][1]=rs.getDate(2);
                objectData[i][2]=rs.getString(3);
                objectData[i][3]=rs.getDate(4);
                objectData[i][4]=rs.getDate(5); 
                objectData[i][5]=rs.getString(6);
                objectData[i][6]=rs.getInt(7);
                       
                objectData[i][7]=rs.getInt(8);
                objectData[i++][8]=rs.getString(9);
               
         }
              /*   String s1=rs.getString(2);
                String s2=rs.getString(4);
                if(s1.compareTo(s2)==0)System.out.println("Hai chuoi do bang nhau do");*/
                tableData =new JTable(objectData,columnNames);
                Container contenPane=this.getContentPane();
                contenPane.add(new JScrollPane(tableData),"Center");
                this.setBounds(200,200 ,1200, 500);
                this.show(true);
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
    }
     public void actionPerformed(ActionEvent e)
    {
    	try{
    		if(e.getSource().equals(btnAdd))
    		{
                
                String s1=txtNgdk.getText();
                String s2=txtNgden.getText();
                String s3=txtNgdi.getText();
                
                if(s1.compareTo(s2)<=0 && s2.compareTo(s3)<=0)
                {
            	String sq="insert into dangki values ('"+txtSodk.getText();sq=sq+"','"+txtNgdk.getText()
                ;sq=sq+"','"+txtMakh.getText();sq=sq+"','"+ txtNgden.getText();sq=sq+"','" +txtNgdi.
                getText();sq=sq +"','" +txtRoom.getText();sq=sq + "','"+Integer.parseInt(txtSong.getText())
                ;sq=sq+"','" +Integer.parseInt(txtDtruoc.getText());sq=sq+"','"+txtHoten.getText()+"')";
                st.executeUpdate(sq);
                String sq1="update danhsachphong set tinhtrang='DA THUE'where phong ='"+txtRoom.getText() + "'";
                st.executeUpdate(sq1);
                xuatbangdl();
                
     		JOptionPane.showMessageDialog(this,"Da them vao danh sach!"); 
                }
                 if(s1.compareTo(s2)>0)                    
                {
                    JOptionPane.showMessageDialog(this,"Ngay den phai lon " +
                        "hon hay bang ngay dang ky! nhap lai di "); 
                }
                if(s2.compareTo(s3)>0)
                {
                    JOptionPane.showMessageDialog(this,"Ngay den phai nho " +
                        "hon hay bang ngay di! nhap lai di "); 
                }
                               
                                
    		}
              
    		else if (e.getSource().equals(btnDelete))
                {
                  
                    //String sq="delete from pthuephong where hoten='"+txthten.getText()+"'";
                    String sq1="delete from hdphong where sodk='"+txtSodk.getText()+"'";
                    st.executeUpdate(sq1);
                    String sq2="delete from traphong where sodk='"+txtSodk.getText()+"'";
                    st.executeUpdate(sq2);
                    String sq3="delete from nhanphong where sodk='"+txtSodk.getText()+"'";
                    st.executeUpdate(sq3);
                    
                    String sq= "delete  from dangki where sodk ='"+txtSodk.getText()+"'";
                    st.executeUpdate(sq);
                    String sql="update danhsachphong set tinhtrang='TRONG'where phong =" +
                            "'"+txtRoom.getText() + "'";
                    st.executeUpdate(sql);
                    xuatbangdl();
                    
                }
                else if(e.getSource().equals(btnRepair))
                {   
                    String sql= "update dangki set ngaydk='"+txtNgdk.getText()+"',makh='" +
                    txtMakh.getText()+"',ngayden='"+txtNgden.getText()+"',ngaydi='"+txtNgdi.getText()+"'" +
                    ",phong='"+txtRoom.getText()+"',songuoi='"+ txtSong.getText()+"',duatruoc='"+txtDtruoc.getText()+"',hoten='"+txtHoten.getText()+"' " +
                    " where sodk= '"+txtSodk.getText()+"'";       
                    st.executeUpdate(sql);
                    xuatbangdl();
                }
                else if(e.getSource().equals(btnExit))
                {
                    this.dispose();
                //	System.exit(0);
                }
        
        }
    	catch(SQLException ce)
    	{
    		System.out.println(ce);
    	}
    	
    	
    }
    
}
