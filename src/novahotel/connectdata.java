/*
 * connectdata.java
 *
 * Created on May 19, 2006, 2:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package novahotel;

/**
 *
 * @author nova
 */
import java.lang.*;
import java.sql.*;
public class connectdata {
    public  String url = null;
    public  Statement stm ;
    public  Connection conn;
    public  ResultSet rs ;
    public ResultSetMetaData rsmd ;
    
    /** Creates a new instance of connectdata */
    public connectdata() throws SQLException{
        new sun.jdbc.odbc.JdbcOdbcDriver();
        conn =java.sql.DriverManager.getConnection("jdbc:odbc:NOVAHOTEL");
        stm= conn.createStatement();
        rs = null;
        rsmd =null;
    }
    public void getTable(String nameTable) throws SQLException { 
        rs=stm.executeQuery("select*from "+nameTable);
        rsmd= rs.getMetaData();
    }
    public void deleteData(String stringDelete) throws SQLException { 
        stm.executeUpdate(stringDelete);
    }
    
    public void insertData(String stringInsert) throws SQLException{ 
         stm.executeUpdate(stringInsert);
    }
    public void updateData (String stringUpdate) throws SQLException { 
         stm.executeUpdate(stringUpdate);
    }
    public void stopDatabase() throws SQLException { 
        stm.close();
        conn.close();
        rs.close();
    }
    
    
}
